# kubernetes-flux
The kubernetes cluster that I run in my house on 4 raspberry pis.

## Commands

### Ansible
- Deploy k3s: `ansible-playbook ansible/setup.yml -i ansible/inventory`
- Destroy Cluster: `ansible-playbook ansible/destroy.yml -i ansible/inventory`

### Flux
- Bootstrap: `flux bootstrap git --url=ssh://git@gitea.com/jojotastic777/kubernetes-flux.git --path ./cluster/core --private-key-file ~/.ssh/id_rsa`
- Deploy Apps: `flux create kustomization apps --path ./cluster/apps --source GitRepository/flux-system`
